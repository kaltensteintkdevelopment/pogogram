## PogoGram - Pokémon Go Notification Bot

PogoGram is a bot written for [Pokémon Go](https://pokemongo.com).

### What will this do?
Using the unofficial [Python](https://www.python.org) API, it scans the defined area and reports any encountered pokemon matching your criteria to a [Telegram](https://www.telegram.org) group. I even might add other services like [Pushbullet](https://www.pushbullet.com), [Join](https://joaoapps.com/join/) or others, when this works properly.

For additional data mining, it will save all encounters and spawn points into a predefined [MySQL](https://www.mysql.org)-/[MariaDB](https://mariadb.org) database, so you can further process stored data.

### But doesn't &lt;insert map software of choice here&gt; do all that already?

Yes, of course. But when I got into doing this, most required a middleware like PokéAlert to do this.

### TL;DR please?

PogoGram is a software built upon [pgoapi](https://github.com/pogodevorg/pgoapi/), scanning a predefined area for Pokémon, then posting it to a Telegram group and saving it into a MySQL database for further processing.

### Requirements
#### Absolute
* [Python](https://www.python.org) > 3.2
* [MySQL](https://www.mysql.org)/[MariaDB](https://www.mariadb.org)
#### Optional
* a linux server (this is developed on Python v3.6.1 on Arch Linux)

### I can haz download plz?

Once it is ready. You can participate in development by using the develop branch, reporting issues, commit pull requests and all that jazz.
